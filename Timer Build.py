import time
import pickle
print("Would you like to load a previous workout?")
load = input()

if load == "Yes":
       workout = input("Which workout would you like to load? ")

       pickle_in = open("dict." + workout, "rb")
       workout_dict = pickle.load(pickle_in)

       pickle_in.close()

       print(workout_dict)

       set_count = int(workout_dict.get("set"))
       while set_count > 0:
               #timer for work time
               print("WORK!!!")
               try:
                       when_to_stop = abs(int(workout_dict.get("work")))

               except KeyboardInterrupt:
                       break
               except:
                       print("Not a number!")

               while when_to_stop > 0:
                       m, s = divmod(when_to_stop, 60)
                       time_left = (str(m).zfill(2) + ":" + str(s).zfill(2))
                       print(time_left)
                       time.sleep(1)
                       when_to_stop -= 1
               #timer for rest time
               print("rest")
               try:
                       when_to_stop = abs(int(workout_dict.get("rest")))

               except KeyboardInterrupt:
                       break
               except:
                       print("Not a number!")

               while when_to_stop > 0:
                       m, s = divmod(when_to_stop, 60)
                       time_left = (str(m).zfill(2) + ":" + str(s).zfill(2))
                       print(time_left)
                       time.sleep(1)
                       when_to_stop -= 1
               set_count -= 1 #counts down the set
               if set_count > 0:
                       print(str(set_count)+ " sets left!")
               else:
                       print("DONE!!!")


if load == "No":
       print("Lets progtam an new workout!!!")

       # input for sets, work time, and rest time
       sets = input("How many sets? ") 
       work_time = input("How long is your work time? ")
       rest_time = input("How long is your rest time? ")
       set_count = (abs(int(sets)))
       save = input("Would you like to save this workout? ")
       if save == 'Yes':
              workout = {"set": sets, "work": work_time, "rest": rest_time}
              file_name = input("What would you like to name the workout?")
              pickle_out = open("dict." + file_name, "wb")
              pickle.dump(workout, pickle_out)
              pickle_out.close()
              
       while set_count > 0:
               #timer for work time
               print("WORK!!!")
               try:
                       when_to_stop = abs(int(work_time))

               except KeyboardInterrupt:
                       break
               except:
                       print("Not a number!")

               while when_to_stop > 0:
                       m, s = divmod(when_to_stop, 60)
                       time_left = (str(m).zfill(2) + ":" + str(s).zfill(2))
                       print(time_left)
                       time.sleep(1)
                       when_to_stop -= 1
               #timer for rest time
               print("rest")
               try:
                       when_to_stop = abs(int(rest_time))

               except KeyboardInterrupt:
                       break
               except:
                       print("Not a number!")

               while when_to_stop > 0:
                       m, s = divmod(when_to_stop, 60)
                       time_left = (str(m).zfill(2) + ":" + str(s).zfill(2))
                       print(time_left)
                       time.sleep(1)
                       when_to_stop -= 1
               set_count -= 1 #counts down the set
               if set_count > 0:
                       print(str(set_count)+ " sets left!")
               else:
                       print("DONE!!!")
